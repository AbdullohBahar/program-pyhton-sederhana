import  time

def list():
    print("                                  CENTRAL LIBRARY                           ")
    print("                               DEVIL GAMA HIGH SCHOOL                       ")
    print("============================================================================")
    print("Welcome To Central Library                                                  ")

    print("----------------------------------------------------------------------------")
    print("         kode                 jenis buku                                    ")
    print("----------------------------------------------------------------------------")
    print("          01                  fiksi                                         ")
    print("          02                  non fiksi                                     ")
    print("          03                  sejarah                                       ")
    print("          04                  karya ilmiah                                  ")
    print("          05                  ensiklopedia                                  ")
    print("          06                  kamus bahasa                                  ")
    print("          07                  dongeng                                       ")
    print("          08                  biografi                                      ")
    print("          09                  komik                                         ")
    print("          10                  antologi                                      ")
    print("----------------------------------------------------------------------------")

# menyimpan data pendaftar dan mengubah huruf kapital menjadi huruf kecil
dataNama=[ x.lower() for x in ['Reza','Ita','bahar']]
# menyimpan data peminjam
namaPeminjam = []
kelasPeminjam = []
NoHpPeminjam = []

def awal():
    menu = int(input("1. Pendaftaran \n2. Pendataan peminjam \nMasukkan pilihan menggunakan angka [1/2] : "))
    if menu == 1:
        daftar()
    elif menu == 2:
        peminjaman()
    else:
        print("Anda salah memasukkan menu")
        awal()
def daftar():
    print("-------------------- Pendaftaran --------------------")
    dataPendaftar = input("Masukkan nama: ")
    dataNama.append(dataPendaftar)
    input("--------------- Pendaftaran berhasil dilakukan ---------------")
    peminjaman()

def peminjaman():
    # untuk memanggil list buku
    list()

    # input nama dam kelas
    nama = input("Nama :")
    namaPeminjam.append(nama)
    namaKecil = nama.lower();
    if namaKecil not in dataNama :
        input(f"Maaf {namaKecil} tidak ada di dalam data kami, harap lakukan pendaftaran")
        daftar()

    kelas = input("Kelas :")
    kelasPeminjam.append(kelas)
    noHp = input("Nomor HP: ")
    NoHpPeminjam.append(noHp)
    ulang()

def ulang():
    print("----------------------------------------------------------------------------")

    banyak_jenis = int(input("banyak jenis:"))
    kode_buku = []
    jenis_buku = []
    judul_buku = []

    i = 0
    while i < banyak_jenis:
        print("jenis ke-=", i + 1)

        kode_buku.append(input("kode buku [01/02/03/04/05/06/07/08/09/10] :"))
        judul_buku.append(input("judul buku:"))

        if kode_buku[i] == "01" or kode_buku[i] == "01":
            jenis_buku.append("fiksi")
        elif kode_buku[i] == "02" or kode_buku[i] == "02":
            jenis_buku.append("non fiksi")
        elif kode_buku[i] == "03" or kode_buku[i] == "03":
            jenis_buku.append("sejarah")
        elif kode_buku[i] == "04" or kode_buku[i] == "04":
            jenis_buku.append("karya ilmiah")
        elif kode_buku[i] == "05" or kode_buku[i] == "05":
            jenis_buku.append("ensiklopedia")
        elif kode_buku[i] == "06" or kode_buku[i] == "06":
            jenis_buku.append("kamus bahasa")
        elif kode_buku[i] == "07" or kode_buku[i] == "07":
            jenis_buku.append("dongeng")
        elif kode_buku[i] == "08" or kode_buku[i] == "08":
            jenis_buku.append("biografi")
        elif kode_buku[i] == "09" or kode_buku[i] == "09":
            jenis_buku.append("komik")
        elif kode_buku[i] == "10" or kode_buku[i] == "10":
            jenis_buku.append("antologi")
        else:
            jenis_buku.append("kode salah")

        i = i + 1

    print("--------------------------------------------------------------------------------")
    print("Nama :", namaPeminjam[0])
    print("Kelas :", kelasPeminjam[0])
    print("Nomor HP:", NoHpPeminjam[0])
    print("--------------------------------------------------------------------------------")
    print("                         Central Library                                        ")
    print("                      Devil Gama High School                                    ")
    print("--------------------------------------------------------------------------------")
    print("        No                       jenis buku                  judul buku         ")
    print("--------------------------------------------------------------------------------")

    a = 0
    while a < banyak_jenis:
        print("   %i                         %s                          %s  " % (a + 1, jenis_buku[a], judul_buku[a]))
        a = a + 1
    print("-------------------------------------------------------------------------------")

    localtime = time.localtime(time.time())
    tanggal_peminjaman = input("tanggal peminjaman :")
    tanggal_pengembalian = input("tanggal pengembalian:")

    lamapinjam = int(input("Lama Pinjam:"))
    if lamapinjam >= 7:
        denda = lamapinjam * 1000
        print("denda Rp ",denda)
    else:
        print("tidak denda")

awal()